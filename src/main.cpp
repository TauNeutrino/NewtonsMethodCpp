//-------------------------------------------
// Project NewtonsMethodCpp
//-------------------------------------------
#include <cstdlib>
#include <iostream>
#include "../hdr/NewtonsMethodCpp.hpp"
namespace ve6ji {
  // Use namespace to avoid confusion with other libraries
  double sqrt(double x, double eps);
  double abs(double x);
} // namespace ve6ji

int main(int argc, char const *argv[]){
  std::cout.precision(12);
  // Value to find the square root of enetered in command line
  double value = std::atof(argv[1]); // ./newton {your value here} //
  D("Value entered: ", value);
  // Epsilon: Set your desired precision
  double epsilon = 0.000000001;
  ve6ji::sqrt(value, epsilon);  
  return 0;
}//main()
 double ve6ji::sqrt(double x, double eps) {
 try{
    std::cout.precision(24); // Show desired precision
    double result = 0.0;
    double x_old = x - (x / 1.5); // Initial guess.  Play with this
    double x_new = 0.0;
    double diff = 0.0;
    int iter = 30; // Number of iterations to calculate
    int i = 0;

    while (i < iter && x_old > 0.0) {
      x_new = 0.5 * (x_old + x / x_old);
      diff = ve6ji::abs(x_old - x_new);
      if (diff < eps) {
        result = x_new;
        break; // Break out of while loop
      }
      x_old = x_new; // set old estimation value
      // std::cout << "Iteration #" << i << "is: " << x_old << std::endl;
      i++;
    }
    std::cout.precision(6);
    D("The square root is: ", result);
    std::cout  << result << std::endl;
    return result;
  } catch (...) {
    return 1;
  }
}

double ve6ji::abs(double x) {
  // Returns the absolute value
  if (x == 0 || x > 0) {
    return x;
  }
  if (x < 0) {
    return (x * -1.0);
  } else {
    return 0.0; // to satisfy compiler warning
  }
}

