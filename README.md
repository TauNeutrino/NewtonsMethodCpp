# NewtonsMethodCpp
## DESCRIPTION
### By: TauNeutrino
- This project's repository: [git@codeberg.org:TauNeutrino/NewtonsMethodCpp.git]
- Edit this README

--- 

Directory Tree
<pre>
/home/tau/projects/Cpp/NewtonsMethodCpp
|-- hdr
|   `-- NewtonsMethodCpp.hpp
|-- src
|   `-- main.cpp
|-- makefile
`-- README.md

2 directories, 4 files
</pre>